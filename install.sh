docker build -t qt5clanggpp . | tee .build.log
TAG=`cat .build.log | tail -n1 | awk 'NF>1{printf $NF}'`
docker tag $TAG agilob/qt5clanggpp:latest
docker push agilob/qt5clanggpp
